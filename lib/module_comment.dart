library comment;

export 'src/service/service.dart';
export 'src/service/comment_service.dart';
export 'src/service/comment_service_firebase_default_impl.dart';

export 'src/provider/comment_model.dart';

export 'src/screen/comment_screen.dart';
