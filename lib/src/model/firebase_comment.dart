import 'package:comment/src/model/comment.dart';
import 'package:models/module_models.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

EventObject<Comment> convertCommentDocChangeToEvent(
    DocumentChange itemChanged) {
  print('Phungtd: convertCommentDocChangeToEvent: type: ${itemChanged.type}');
  Comment comment = Comment.fromJson(itemChanged.doc.data()!);
  comment.docId = itemChanged.doc.id;

  bool hasPendingWrites = itemChanged.doc.metadata.hasPendingWrites;

  switch (itemChanged.type) {
    case DocumentChangeType.added:
      return EventObject(data: comment, type: ChangeType.added);
    case DocumentChangeType.modified:
      return EventObject(
        data: comment,
        type: ChangeType.modified,
        metadata: FirebaseObjectMetadata(hasPendingWrites: hasPendingWrites),
      );
    case DocumentChangeType.removed:
      return EventObject(data: comment, type: ChangeType.removed);
  }
}
