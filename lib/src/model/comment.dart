import 'package:comment/src/constants.dart';
import 'package:comment/src/model/user.dart';

class Comment {
  String docId;
  bool isReply = false;
  String id;
  String content = "";
  DateTime createdDate;
  DateTime lastUpdate;
  List<String> like = [];
  int type;
  List<Comment> replyComment = [];
  UserInfo? userInfo;
  String parentId;
  String rootId;
  String hashtag = "";
  List<String> images = [];
  List<String> sounds = [];
  List<String> videos = [];

  Comment(
      {this.id = "",
      this.content = "",
      DateTime? createdDate,
      DateTime? lastUpdate,
      this.like = const [],
      this.type = COMMENT_TYPE_PARENT,
      this.replyComment = const [],
      this.userInfo,
      this.parentId = "",
      this.rootId = "",
      this.docId = "",
      this.images = const [],
      this.sounds = const [],
      this.videos = const [],
      this.hashtag = ""})
      : this.createdDate = createdDate ?? DateTime.now(),
        this.lastUpdate = lastUpdate ?? DateTime.now();

  Comment.fromJson(Map<String, dynamic> json)
      : id = json['id'] as String,
        docId = '',
        content = json['content'] as String,
        createdDate =
            DateTime.fromMillisecondsSinceEpoch(json['createdDate'] as int),
        lastUpdate =
            DateTime.fromMillisecondsSinceEpoch(json['lastUpdate'] as int),
        like = (json['like'] as List).map((e) => e as String).toList(),
        type = json['type'] as int,
        userInfo = json['userInfo'] == null
            ? null
            : UserInfo.fromJson(json['userInfo'] as Map<String, dynamic>),
        parentId = json['parentId'] as String,
        rootId = json['rootId'] as String,
        images = (json['images'] as List).map((e) => e as String).toList(),
        sounds = (json['sounds'] as List).map((e) => e as String).toList(),
        videos = (json['videos'] as List).map((e) => e as String).toList(),
        hashtag = json['hashtag'] as String;

  toJson() {
    return <String, dynamic>{
      'id': id,
      'content': content,
      'createdDate': createdDate.millisecondsSinceEpoch,
      'lastUpdate': lastUpdate.millisecondsSinceEpoch,
      'like': like,
      'type': type,
      'userInfo': userInfo?.toJson(),
      'parentId': parentId,
      'rootId': rootId,
      'hashtag': hashtag,
      'images': images,
      'sounds': sounds,
      'videos': videos,
    };
  }
}
