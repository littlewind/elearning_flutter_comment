class UserInfo {
  String? docId;
  String userName;
  String id = "";
  String fullName;
  String password;
  String avatar;
  int role;


  UserInfo({
    this.userName = "",
    this.fullName = "",
    this.password = "",
    this.avatar = "",
    this.role = 0,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) =>
      _$UserInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoToJson(this);
}

UserInfo _$UserInfoFromJson(Map<String, dynamic> json) {
  return UserInfo(
    userName: json['userName'] as String,
    fullName: json['fullName'] as String,
    password: json['password'] as String,
    avatar: json['avatar'] as String,
    role: json['role'] as int,
  )..id = json['id'] as String;
}

Map<String, dynamic> _$UserInfoToJson(UserInfo instance) => <String, dynamic>{
  'userName': instance.userName,
  'id': instance.id,
  'fullName': instance.fullName,
  'password': instance.password,
  'avatar': instance.avatar,
  'role': instance.role,
};
