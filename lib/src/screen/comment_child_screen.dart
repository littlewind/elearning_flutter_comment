import 'dart:ui';
import 'package:comment/src/screen/comment_item.dart';
import 'package:comment/src/constants.dart';
import 'package:comment/src/model/comment.dart';
import 'package:comment/src/model/user.dart';
import 'package:comment/src/provider/comment_model.dart';
import 'package:comment/src/service/comment_service.dart';
import 'package:comment/src/service/service.dart';
import 'package:flutter/material.dart';
import 'package:models/module_models.dart';
import 'package:provider/provider.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

const double defaultPadding = 24.0;

class CommentChildScreen extends StatefulWidget {
  final Comment parentComment;

  CommentChildScreen({required this.parentComment});

  @override
  _CommentChildScreenState createState() => _CommentChildScreenState();
}

class _CommentChildScreenState extends State<CommentChildScreen> {
  Comment get parentComment => widget.parentComment;
  late CommentModel commentModel;
  late DynamicsColor dynamicsColor = DynamicsColor.of<CustomColor>(context);
  final _commentFormKey = GlobalKey<FormState>();
  final TextEditingController commentTextController = TextEditingController();
  ScrollController _scrollController = new ScrollController();

  late UserInfo userInfo;
  FocusNode commFocus = FocusNode();
  bool isModify = false;
  Comment? modifyComm;

  late CommentService _commentService =
      CommentServiceInitializer().commentService;

  @override
  void initState() {
    super.initState();
    commentModel = Provider.of(context, listen: false);
    userInfo = context.read<CommentModel>().userInfo;

    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      commFocus.requestFocus();
      scrollToBottom();
    });
  }

  void scrollToBottom() {
    _scrollController.animateTo(
      _scrollController.position.maxScrollExtent + 300,
      curve: Curves.easeOut,
      duration: const Duration(milliseconds: 300),
    );
  }

  bool checkIsLiked(Comment _comment) {
    UserInfo userInfo = context.read<CommentModel>().userInfo;
    if (_comment.like.isEmpty || !_comment.like.contains(userInfo.id)) {
      return false;
    } else {
      return true;
    }
  }

  delete(Comment comm) {
    commentModel.delete(comm);
  }

  like(Comment comm) {
    UserInfo userInfo = context.read<CommentModel>().userInfo;
    commentModel.like(comm, userInfo.id);
  }

  reply(Comment parentComment, String _content, bool _isMoify,
      Comment? _modifyComm) {
    if (_isMoify && _modifyComm != null) {
      _modifyComm.content = _content;
      commentModel.modify(_modifyComm.docId, _modifyComm);
    } else {
      UserInfo userInfo = context.read<CommentModel>().userInfo;
      Comment temp = Comment();
      temp
        ..parentId = parentComment.id
        ..rootId = _commentService.getRootId()
        ..content = _content
        ..createdDate = DateTime.now()
        ..id =
            DateTime.now().millisecondsSinceEpoch.toString() + "_" + userInfo.id
        ..type = COMMENT_TYPE_REPLY
        ..userInfo = userInfo
        ..lastUpdate = DateTime.now();
      commentModel.reply(parentComment.id, temp);
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          unFocus();
        },
        child: Stack(
          children: [
            Container(
              child: Column(
                children: [
                  Container(
                    height: size.height * 0.25 - 27,
                    padding: EdgeInsets.only(
                      bottom: 36,
                    ),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color(0xff49BEB7),
                          Color(0xff085F63),
                        ],
                        stops: [-.58, 1],
                        tileMode: TileMode.clamp,
                        begin: FractionalOffset.topLeft,
                        end: FractionalOffset.bottomRight,
                        transform: GradientRotation(-180),
                      ),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.elliptical(25, 25),
                        bottomRight: Radius.elliptical(25, 25),
                      ),
                    ),
                    child: Container(
                      // color: Colors.red,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          IconButton(
                            icon: Icon(
                              Icons.arrow_back_ios,
                              color: Theme.of(context).backgroundColor,
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          Expanded(
                            child: Container(
                              child: Center(
                                child: Text(
                                  "Communication",
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      ?.copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 24),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 25,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // color: Colors.red,
              child: Column(
                children: [
                  // _buildSearch(),
                  SizedBox(
                    height: size.height * 0.15,
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      margin: EdgeInsets.only(bottom: 20),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(30),
                        child: BackdropFilter(
                          filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                          child: Container(
                            padding: EdgeInsets.all(defaultPadding)
                                .copyWith(bottom: 0),
                            decoration: BoxDecoration(
                                color: dynamicsColor
                                    .customColor.commentPanelColor),
                            child: SingleChildScrollView(
                              controller: _scrollController,
                              child: Column(
                                children: [
                                  StreamBuilder(
                                    stream: _commentService
                                        .getCommentListSnapshotsByParentId(
                                            parentComment.id),
                                    builder: (_,
                                        AsyncSnapshot<List<Comment>> snapShot) {
                                      if (!snapShot.hasData) {
                                        return SizedBox();
                                      } else {
                                        var length = snapShot.data!.length;
                                        bool isLike =
                                            checkIsLiked(parentComment);
                                        return Column(
                                          children: [
                                            Container(
                                              child: Text(
                                                "Replies ($length)",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 18),
                                              ),
                                            ),
                                            Container(
                                              child: CommentItem(
                                                comment: parentComment,
                                                showContent: true,
                                                isLike: isLike,
                                                userId: userInfo.id,
                                                dynamicsColor: dynamicsColor,
                                                like: (comm) {
                                                  like(comm);
                                                },
                                                reply: null,
                                                onLongPress: (comm) {
                                                  displayModalBottomSheet(
                                                      context, comm);
                                                },
                                              ),
                                            ),
                                          ],
                                        );
                                      }
                                    },
                                  ),
                                  Consumer<CommentModel>(
                                    builder: (_, CommentModel model, __) {
                                      return StreamBuilder(
                                        stream: _commentService
                                            .getCommentListSnapshotsByParentId(
                                                parentComment.id),
                                        builder: (_,
                                            AsyncSnapshot<List<Comment>>
                                                snapShot) {
                                          if (snapShot.hasError) {
                                            print(snapShot.error);
                                            return Text('Something went wrong');
                                          }

                                          if (snapShot.connectionState ==
                                              ConnectionState.waiting) {
                                            return Container(
                                              width: double.infinity,
                                              child: Text("Loading"),
                                            );
                                          }
                                          if (!snapShot.hasData) {
                                            return SizedBox();
                                          } else {
                                            return Column(
                                              children: snapShot.data!.map((e) {
                                                Comment item = e;
                                                bool isLike =
                                                    checkIsLiked(item);

                                                // bool isLiked =
                                                //     logic.checkIsLiked(item);
                                                return Container(
                                                  margin: EdgeInsets.only(
                                                      left: defaultPadding),
                                                  child: CommentItem(
                                                    comment: item,
                                                    showContent: true,
                                                    dynamicsColor:
                                                        dynamicsColor,
                                                    userId: userInfo.id,
                                                    isLike: isLike,
                                                    like: (comm) {
                                                      like(comm);
                                                    },
                                                    onLongPress: (comm) {
                                                      displayModalBottomSheet(
                                                          context, comm);
                                                    },
                                                    reply: null,
                                                  ),
                                                );
                                              }).toList(),
                                            );
                                          }
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    // color: Colors.transparent,
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Form(
                      key: _commentFormKey,
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: TextFormField(
                          focusNode: commFocus,
                          validator: (text) {
                            if (text == null || text.isEmpty) {
                              return "";
                            } else
                              return null;
                          },
                          onTap: () {
                            scrollToBottom();
                          },
                          controller: commentTextController,
                          keyboardType: TextInputType.multiline,
                          maxLines: 4,
                          textInputAction: TextInputAction.newline,
                          decoration: InputDecoration(
                            hintText: "Write your comment here...",
                            hintStyle: TextStyle(
                              color: Theme.of(context)
                                  .disabledColor
                                  .withOpacity(.5),
                            ),
                            suffixIcon: InkWell(
                              onTap: () {
                                if (_commentFormKey.currentState!.validate()) {
                                  reply(
                                      parentComment,
                                      commentTextController.text,
                                      isModify,
                                      modifyComm);
                                  commentTextController.clear();
                                } else {}
                                unFocus();
                              },
                              child: Icon(
                                FontAwesomeIcons.solidPaperPlane,
                                color:
                                    dynamicsColor.customColor.ieltsPrimaryColor,
                              ),
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15),
                              borderSide: BorderSide.none,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void displayModalBottomSheet(BuildContext context, Comment _comment) {
    String userId = context.read<AppSettingModel>().appSetting!.userId;
    bool isOwner = (_comment.userInfo?.id ?? "") == userId;
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext bc) {
        return Container(
          padding: EdgeInsets.only(left: 15, right: 15, bottom: 30),
          child: Wrap(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: Wrap(
                  children: <Widget>[
                    isOwner
                        ? InkWell(
                            onTap: () async {
                              Navigator.of(context).pop();
                              commentTextController.text = _comment.content;
                              isModify = true;
                              modifyComm = _comment;
                              commFocus.requestFocus();
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              width: double.infinity,
                              child: Center(
                                child: Text(
                                  "Modify",
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      ?.copyWith(
                                          color: dynamicsColor
                                              .customColor.ieltsAppbarColor),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),
                    Divider(
                      color: Colors.grey,
                      height: 1,
                      thickness: .7,
                    ),
                    isOwner
                        ? InkWell(
                            onTap: () {
                              unFocus();
                              Navigator.of(context).pop();
                              delete(_comment);
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              width: double.infinity,
                              child: Center(
                                child: Text(
                                  "Delete",
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      ?.copyWith(
                                        color: Colors.red,
                                      ),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
              Container(
                height: 20,
                color: Colors.transparent,
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  width: double.infinity,
                  child: Center(
                    child: Text(
                      "Canceled",
                      style: Theme.of(context).textTheme.subtitle1?.copyWith(
                          color: dynamicsColor.customColor.ieltsAppbarColor),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  unFocus() {
    try {
      commFocus.unfocus();
      isModify = false;
      modifyComm = null;
    } catch (e) {}
  }
}
