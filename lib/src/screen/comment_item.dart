import 'package:comment/src/constants.dart';
import 'package:comment/src/model/comment.dart';
import 'package:comment/src/service/comment_service.dart';
import 'package:comment/src/service/service.dart';
import 'package:flutter/material.dart';
import 'package:models/module_models.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:timeago/timeago.dart' as timeago;

class CommentItem extends StatefulWidget {
  final Comment comment;
  final DynamicsColor dynamicsColor;
  final Function? like;
  final Function? reply;
  final bool isLike;
  final String userId;
  final Function? onLongPress;
  final Function? onLongPressStart;
  final bool showContent;

  CommentItem({
    required this.comment,
    required this.dynamicsColor,
    required this.userId,
    this.onLongPress,
    this.onLongPressStart,
    this.like,
    this.reply,
    this.showContent = false,
    this.isLike = false,
  });

  @override
  _CommentItemState createState() => _CommentItemState();
}

class _CommentItemState extends State<CommentItem> {
  Color backgroundColor = Colors.transparent;

  Comment get commet => widget.comment;

  bool get showContent => widget.showContent;

  late CommentService _commentService =
      CommentServiceInitializer().commentService;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onLongPress: () async {
        if (widget.onLongPress != null) {
          widget.onLongPress!(widget.comment);
          await Future.delayed(Duration(milliseconds: 300));
          setState(() {
            backgroundColor = Colors.transparent;
          });
        }
      },
      onLongPressStart: (detail) {
        setState(() {
          backgroundColor = Colors.grey;
        });
      },
      onLongPressEnd: (detail) {},
      child: LimitedBox(
        maxHeight: !showContent ? size.height * .22 : size.height * .6,
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5),
          width: double.infinity,
          color: backgroundColor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _buildAvatar(),
              SizedBox(
                width: 8,
              ),
              Expanded(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _buildUserName(context),
                  _buildCommentContent(context),
                  SizedBox(
                    height: 5,
                  ),
                  _buildActions(context, widget.isLike),
                ],
              ))
            ],
          ),
        ),
      ),
    );
  }

  _buildActions(BuildContext context, bool _isLike) {
    final now = new DateTime.now();
    final difference = now.difference(widget.comment.createdDate);
    String textTime = timeago.format(now.subtract(difference));
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          StreamBuilder(
            stream:
                _commentService.getCommentSnapshotsById(widget.comment.id),
            builder: (_, AsyncSnapshot<Comment> snapShot) {
              if (!snapShot.hasData) {
                return SizedBox();
              } else {
                Comment x = snapShot.data!;
                bool isLiked = x.like.contains(widget.userId);

                return InkWell(
                  onTap: () {
                    if (widget.like != null) {
                      widget.like!(widget.comment);
                    }
                  },
                  child: Container(
                      width: 30,
                      height: 30,
                      child: Icon(
                        isLiked
                            ? FontAwesomeIcons.solidHeart
                            : FontAwesomeIcons.heart,
                        size: 14,
                        color:
                            widget.dynamicsColor.customColor.ieltsAppbarColor,
                      ),),
                );
              }
            },
          ),
          SizedBox(
            width: 3,
          ),
          StreamBuilder(
            stream:
                _commentService.getCommentSnapshotsById(widget.comment.id),
            builder: (_, AsyncSnapshot<Comment> snapShot) {
              if (!snapShot.hasData) {
                return _buildNumberText(context, '0');
              } else {
                if (snapShot.data == null) {
                  return _buildNumberText(context, '0');
                }
                Comment x = snapShot.data!;
                return _buildNumberText(context, '${x.like.length}');
              }
            },
          ),
          SizedBox(
            width: 6,
          ),
          widget.comment.type == COMMENT_TYPE_REPLY
              ? SizedBox()
              : Text(
                  "-",
                  style: Theme.of(context).accentTextTheme.subtitle2,
                ),
          SizedBox(
            width: 6,
          ),
          widget.comment.type == COMMENT_TYPE_REPLY
              ? SizedBox()
              : Container(
                  child: InkWell(
                    onTap: () {
                      if (widget.reply != null) {
                        widget.reply!(widget.comment);
                      }
                    },
                    child: Icon(
                      FontAwesomeIcons.commentDots,
                      size: 14,
                      color: widget.dynamicsColor.customColor.ieltsAppbarColor,
                    ),
                  ),
                ),
          SizedBox(
            width: 6,
          ),
          widget.comment.type == COMMENT_TYPE_REPLY
              ? SizedBox()
              : StreamBuilder(
                  stream: _commentService
                      .getCommentListSnapshotsByParentId(widget.comment.id),
                  builder: (_, AsyncSnapshot<List<Comment>> snapShot) {
                    if (!snapShot.hasData) {
                      return _buildNumberText(context, '0');
                    } else {
                      var length = snapShot.data!.length;
                      return _buildNumberText(context, '$length');
                    }
                  },
                ),
          SizedBox(
            width: 6,
          ),
          widget.comment.type == COMMENT_TYPE_REPLY
              ? SizedBox()
              : Text(
                  "-",
                  style: Theme.of(context).accentTextTheme.subtitle2,
                ),
          SizedBox(
            width: 6,
          ),
          widget.comment.type == COMMENT_TYPE_REPLY
              ? SizedBox()
              : InkWell(
                  onTap: () {
                    if (widget.reply != null) {
                      widget.reply!(widget.comment);
                    }
                  },
                  child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      height: 30,
                      child: Center(
                        child: Text(
                          "Reply",
                          style: Theme.of(context)
                              .accentTextTheme
                              .subtitle2
                              ?.copyWith(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff646464)),
                        ),
                      )),
                ),
          SizedBox(
            width: 6,
          ),
          Text(
            textTime,
            style:
                TextStyle(color: Theme.of(context).disabledColor, fontSize: 12),
          ),
        ],
      ),
    );
  }

  _buildCommentContent(context) {
    return Flexible(
      child: InkWell(
        onTap: () {
          if (widget.reply != null) {
            widget.reply!(widget.comment);
          }
        },
        child: Container(
          width: double.infinity,
          child: Text(
            widget.comment.content,
            textAlign: TextAlign.left,
            overflow: !showContent ? TextOverflow.ellipsis : null,
            maxLines: !showContent ? 3 : null,
          ),
        ),
      ),
    );
  }

  _buildUserName(BuildContext context) {
    return SizedBox(
      height: 44,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            widget.comment.userInfo?.userName ?? '',
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: Theme.of(context).textTheme.subtitle1?.copyWith(
                fontWeight: FontWeight.w600,
                fontSize: 18,
                color: widget.dynamicsColor.customColor.ieltsAppbarColor),
          ),
        ],
      ),
    );
  }

  _buildAvatar() {
    return Container(
      width: 44,
      height: 44,
      padding: EdgeInsets.all(4),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          width: 2,
          color: Color(0xff09959B),
        ),
      ),
      child: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
        ),
        child: Center(
          child: CircleAvatar(
            // child: SvgPicture.asset(icons_avatar),
            child: Icon(Icons.person),
          ),
        ),
      ),
    );
  }

  Widget _buildNumberText(BuildContext context, String content) {
    return Text(
      content,
      style: Theme.of(context)
          .accentTextTheme
          .subtitle2
          ?.copyWith(
          fontSize: 12,
          fontWeight: FontWeight.w500,),
    );
  }
}
