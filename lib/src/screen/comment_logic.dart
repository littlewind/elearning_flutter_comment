import 'package:comment/src/screen/comment_child_screen.dart';
import 'package:comment/src/model/comment.dart';
import 'package:comment/src/model/user.dart';
import 'package:comment/src/provider/comment_model.dart';
import 'package:comment/src/service/comment_service.dart';
import 'package:comment/src/service/service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:models/module_models.dart';
import 'package:provider/provider.dart';

class CommentScreenLogic {
  late BuildContext context;
  late CommentModel commentModel;
  late AppSettingModel appSettingModel;
  String path = '';
  late Topic topic;
  late CommentService _commentService = CommentServiceInitializer().commentService;

  CommentScreenLogic(BuildContext _context, Topic _topic) {
    context = _context;
    topic = _topic;
    commentModel = Provider.of(context, listen: false);
    appSettingModel = Provider.of<AppSettingModel>(context, listen: false);
    // path = "/ksapp/$APP_ID/topics/${topic.id}/comments";
    if (commentModel.comments.isEmpty) {
      setFireBaseChannel(_topic);
      loadComments();
    }
  }

  setFireBaseChannel(Topic topic) {
    _commentService.setUpWhenTopicChanges(topic.id);
    commentModel.listenToPosts();
  }

  Stream<List<Comment>> getCommentListSnapshotsByParentId(String parentId) {
    return _commentService.getCommentListSnapshotsByParentId(parentId);
  }

  loadComments() {
    Future.microtask(() {
      commentModel.getComments();
    });
  }

  dispose() async {
    commentModel.reset();
    if (topic.id != commentModel.parentId) {
      print("need Refresh");
      await Future.delayed(Duration(milliseconds: 300));
      commentModel.refreshParentComm();
    }
  }

  // refreshComment() {
  //   print("REFRESH COMMENT");
  //   setFireBaseChannel(topic);
  //   loadComments();
  // }

  // getCommentCount(String _parentId) {
  //   commentModel.getCommentCount(_parentId);
  // }

  goToReply(Comment _comment) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (_) => CommentChildScreen(parentComment: _comment),
    ));
  }

  like(Comment comm) {
    UserInfo userInfo = context.read<CommentModel>().userInfo;
    commentModel.like(comm, userInfo.id);
  }

  delete(Comment comm) {
    commentModel.delete(comm);
  }

  post(String text, bool isModify, Comment? _modifyComm) {
    if (isModify && _modifyComm != null) {
      _modifyComm.content = text;
      commentModel.modify(_modifyComm.docId, _modifyComm);
    } else {
      UserInfo userInfo = context.read<CommentModel>().userInfo;
      Comment temp = Comment();
      temp
        ..parentId = topic.id
        ..rootId = _commentService.getRootId()
        ..content = text
        ..createdDate = DateTime.now()
        ..id =
            DateTime.now().millisecondsSinceEpoch.toString() + "_" + userInfo.id
        ..userInfo = userInfo
        ..lastUpdate = DateTime.now();
      commentModel.insert(temp);
    }
  }

  // modify(Comment _comm, String _newContent) {}

  bool checkIsLiked(Comment _comment) {
    UserInfo userInfo = context.read<CommentModel>().userInfo;
    if (_comment.like.isEmpty ||
        !_comment.like.contains(userInfo.id)) {
      return false;
    } else {
      return true;
    }
  }

  // Future<void> changeUserName(String _userName) async {
  //   if (_userName.isNotEmpty) {
  //     await appSettingModel.updateUserName(_userName);
  //     UserInfo xx = commentModel.userInfo;
  //     xx.userName = _userName;
  //     await commentModel.updateUserInfo(xx);
  //   }
  // }
}
