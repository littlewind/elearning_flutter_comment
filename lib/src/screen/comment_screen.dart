import 'dart:ui';
import 'package:comment/src/screen/comment_item.dart';
import 'package:comment/src/screen/comment_logic.dart';
import 'package:comment/src/model/comment.dart';
import 'package:comment/src/model/user.dart';
import 'package:comment/src/provider/comment_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:models/module_models.dart';
import 'package:provider/provider.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

const double defaultPadding = 24.0;

class CommentScreen extends StatefulWidget {
  final Topic topic;

  CommentScreen({required this.topic});

  @override
  _CommentScreenState createState() => _CommentScreenState();
}

class _CommentScreenState extends State<CommentScreen>
    with AutomaticKeepAliveClientMixin {
  Topic get topic => widget.topic;
  ScrollController controller = ScrollController();

  // GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  late CommentModel commentModel;
  late DynamicsColor<CustomColor> dynamicsColor =
      DynamicsColor.of<CustomColor>(context);
  late CommentScreenLogic logic;
  final _commentFormKey = GlobalKey<FormState>();
  TextEditingController commentTextController = TextEditingController();
  FocusNode commFocus = FocusNode();
  late UserInfo userInfo;
  bool isModify = false;
  Comment? modifyComm;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      await Future.delayed(Duration(milliseconds: 300));
      controller.addListener(_scrollListener);
    });
    commentModel = Provider.of<CommentModel>(context, listen: false);
    userInfo = context.read<CommentModel>().userInfo;
    // commentModel.addListener(commentListener);
    super.initState();
  }

  // ksapp/5641892340432896/topics/6195004402827264/comments
  // commentListener() {
  //   if (mounted) {
  //     if (commentModel.parentId != null &&
  //         commentModel.refreshParentData &&
  //         commentModel.comments.isEmpty &&
  //         (topic.id == commentModel.parentId)) {
  //       logic.refreshComment();
  //     }
  //   }
  // }

  void _scrollListener() {
    if (controller.offset >= controller.position.maxScrollExtent &&
        !controller.position.outOfRange) {
      bool isLoading = context.read<CommentModel>().isLoading;
      if (!isLoading) {
        logic.loadComments();
      }
    }
  }

  @override
  void didChangeDependencies() async {
    logic = CommentScreenLogic(context, topic);
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant CommentScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    unFocus();
    // commentModel.removeListener(commentListener);
    super.dispose();
    try {
      logic.dispose();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          unFocus();
        },
        child: Stack(
          children: [
            Container(
              // decoration: BoxDecoration(
              //   image: DecorationImage(
              //       image: AssetImage(
              //         "assets/images/list_example_bg.png",
              //       ),
              //       fit: BoxFit.fill),
              // ),
              child: Column(
                children: [
                  Container(
                    height: size.height * 0.25 - 27,
                    padding: EdgeInsets.only(
                      bottom: 36,
                    ),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color(0xff49BEB7),
                          Color(0xff085F63),
                        ],
                        stops: [
                          -.58,
                          1,
                        ],
                        tileMode: TileMode.clamp,
                        begin: FractionalOffset.topLeft,
                        end: FractionalOffset.bottomRight,
                        transform: GradientRotation(-180),
                      ),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.elliptical(25, 25),
                        bottomRight: Radius.elliptical(25, 25),
                      ),
                    ),
                    child: Container(
                      // color: Colors.red,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.arrow_back_ios,
                                color: Theme.of(context).backgroundColor,
                              ),
                              onPressed: () {
                                unFocus();
                                Navigator.of(context).pop();
                              }),
                          Expanded(
                            child: Container(
                              child: Center(
                                child: Text(
                                  "Communication",
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1!
                                      .copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 24),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 25,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              children: [
                // _buildSearch(),
                SizedBox(
                  height: size.height * 0.15,
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    margin: EdgeInsets.only(bottom: 20),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(30),
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.all(defaultPadding),
                          decoration: BoxDecoration(
                              color:
                                  dynamicsColor.customColor.commentPanelColor),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              StreamBuilder(
                                stream: logic.getCommentListSnapshotsByParentId(topic.id),
                                builder: (_,
                                    AsyncSnapshot<List<Comment>> snapShot) {
                                  if (!snapShot.hasData) {
                                    return SizedBox();
                                  } else {
                                    var length = snapShot.data!.length;
                                    return Center(
                                      child: Container(
                                        child: Text(
                                          "Comments ($length)",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 18),
                                        ),
                                      ),
                                    );
                                  }
                                },
                              ),
                              Expanded(
                                child: Consumer<CommentModel>(
                                  builder: (_, CommentModel model, __) {
                                    if (model.isLoading && model.comments.isEmpty) {
                                      return Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }
                                    if (!model.isLoading && model.comments.isEmpty) {
                                      return Container(
                                        child: Text("No comments"),
                                      );
                                    }

                                    return ListView.builder(
                                        padding: EdgeInsets.only(top: 0),
                                        controller: controller,
                                        itemCount: model.comments.length,
                                        itemBuilder: (_, index) {
                                          Comment item = model.comments[index];
                                          bool isLiked =
                                              logic.checkIsLiked(item);
                                          return CommentItem(
                                              comment: item,
                                              dynamicsColor: dynamicsColor,
                                              isLike: isLiked,
                                              userId: userInfo.id,
                                              onLongPress: (comm) {
                                                displayModalBottomSheet(
                                                    context, comm);
                                              },
                                              like: (comm) {
                                                logic.like(comm);
                                              },
                                              reply: (comm) {
                                                // print(comm);
                                                logic.goToReply(comm);
                                              });
                                        });
                                  },
                                ),
                              ),
                              Form(
                                key: _commentFormKey,
                                child: Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).backgroundColor,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: TextFormField(
                                    focusNode: commFocus,
                                    keyboardType: TextInputType.multiline,
                                    maxLines: 4,
                                    textInputAction: TextInputAction.newline,
                                    validator: (text) {
                                      if (text == null || text.isEmpty) {
                                        return "";
                                      } else
                                        return null;
                                    },
                                    controller: commentTextController,
                                    decoration: InputDecoration(
                                      hintText: "Write your comment here...",
                                      hintStyle: TextStyle(
                                        color: Theme.of(context)
                                            .disabledColor
                                            .withOpacity(.5),
                                      ),
                                      suffixIcon: InkWell(
                                        onTap: () {
                                          if (_commentFormKey
                                              .currentState!
                                              .validate()) {
                                            logic.post(
                                                commentTextController
                                                    .text,
                                                isModify,
                                                modifyComm);
                                            commentTextController.clear();
                                          } else {}
                                          unFocus();
                                        },
                                        child: Icon(
                                          FontAwesomeIcons.solidPaperPlane,
                                          color: dynamicsColor
                                              .customColor.ieltsPrimaryColor,
                                        ),
                                      ),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(15),
                                          borderSide: BorderSide.none),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  unFocus() {
    try {
      commFocus.unfocus();
      isModify = false;
      modifyComm = null;
    } catch (e) {}
  }

  void displayModalBottomSheet(BuildContext context, Comment _comment) {
    String userId = context.read<AppSettingModel>().appSetting!.userId;
    bool isOwner = (_comment.userInfo?.id ?? "") == userId;
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext bc) {
        return Container(
          padding: EdgeInsets.only(left: 15, right: 15, bottom: 30),
          child: Wrap(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Wrap(
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        unFocus();
                        Navigator.of(context).pop();
                        logic.goToReply(_comment);
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        width: double.infinity,
                        child: Center(
                          child: Text(
                            "Reply",
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1!
                                .copyWith(
                                    color: dynamicsColor
                                        .customColor.ieltsAppbarColor),
                          ),
                        ),
                      ),
                    ),
                    Divider(
                      color: Colors.grey,
                      height: 1,
                      thickness: .7,
                    ),
                    isOwner
                        ? InkWell(
                            onTap: () async {
                              Navigator.of(context).pop();
                              commentTextController.text = _comment.content;
                              isModify = true;
                              modifyComm = _comment;
                              commFocus.requestFocus();
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              width: double.infinity,
                              child: Center(
                                child: Text(
                                  "Modify",
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1!
                                      .copyWith(
                                          color: dynamicsColor
                                              .customColor.ieltsAppbarColor),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),
                    Divider(
                      color: Colors.grey,
                      height: 1,
                      thickness: .7,
                    ),
                    isOwner
                        ? InkWell(
                            onTap: () {
                              unFocus();
                              Navigator.of(context).pop();
                              logic.delete(_comment);
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 15),
                              width: double.infinity,
                              child: Center(
                                child: Text(
                                  "Delete",
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1!
                                      .copyWith(
                                        color: Colors.red,
                                      ),
                                ),
                              ),
                            ),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
              Container(
                height: 20,
                color: Colors.transparent,
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  width: double.infinity,
                  child: Center(
                    child: Text(
                      "Canceled",
                      style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: dynamicsColor.customColor.ieltsAppbarColor),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  // sendComment() {
  //   unFocus();
  // }
}
