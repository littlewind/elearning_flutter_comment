import 'package:comment/src/service/comment_service.dart';

class CommentServiceInitializer {
  factory CommentServiceInitializer() {
    if (_instance == null) {
      _instance = CommentServiceInitializer._getInstance();
    }
    return _instance!;
  }

  static CommentServiceInitializer? _instance;
  CommentServiceInitializer._getInstance();

  late final CommentService commentService;

  init(CommentService commentService) {
    this.commentService = commentService;
  }
}