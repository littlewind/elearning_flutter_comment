import 'dart:async';
import 'package:comment/src/model/comment.dart';
import 'package:models/module_models.dart';

abstract class CommentService {
  setUpWhenTopicChanges(String topicId);
  String getRootId();
  Stream<List<EventObject>> listenToPostsRealTime();
  Stream<Comment> getCommentSnapshotsById(String id);
  Stream<List<Comment>> getCommentListSnapshotsByParentId(String parentId);
  Future<CommentListWithOffset> getComments<T>(T offset, int limit);
  Future<void> likeComment(Comment dataLike, String userId);
  Future<void> insertComment(Comment comment);
  Future<void> deleteComment(Comment comment);
  Future<void> replyComment(String parentId, Comment data);
  Future<void> modifyComment(String docId, Comment modify);
}

class CommentListWithOffset {
  List<Comment> commentList;
  dynamic offset;

  CommentListWithOffset({required this.commentList, required this.offset});
}