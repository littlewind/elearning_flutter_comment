import 'dart:io';

import 'package:comment/src/constants.dart';
import 'package:comment/src/model/comment.dart';
import 'package:comment/src/model/firebase_comment.dart';
import 'package:comment/src/service/comment_service.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:models/module_models.dart';

class CommentServiceFirebaseDefaultImpl implements CommentService {
  CommentServiceFirebaseDefaultImpl({
    required String androidAppId,
    required String iosAppId,
    required String apiKey,
    required String messagingSenderId,
    required String projectId,
  }) {
    String appId = '';
    if (Platform.isAndroid) {
      appId = androidAppId;
    } else if (Platform.isIOS) {
      appId = iosAppId;
    }
    initFirestore(
      appId: appId,
      apiKey: apiKey,
      messagingSenderId: messagingSenderId,
      projectId: projectId,
    );
  }

  static const String serviceName = 'CommentService';
  late FirebaseFirestore _store;

  FirebaseFirestore get store => _store;
  late CollectionReference ref;

  initFirestore({
    required String appId,
    required String apiKey,
    required String messagingSenderId,
    required String projectId,
  }) async {
    await Firebase.initializeApp(
      name: serviceName,
      options: FirebaseOptions(
        appId: appId,
        apiKey: apiKey,
        messagingSenderId: messagingSenderId,
        projectId: projectId,
      ),
    );
    final commentApp = Firebase.app(serviceName);
    _store = FirebaseFirestore.instanceFor(app: commentApp);
  }

  @override
  String getRootId() {
    return 'APP_ID';
  }

  @override
  setUpWhenTopicChanges(String topicId) {
    final path = "/ksapp/${getRootId()}/topics/$topicId/comments";
    setPath(path);
  }

  setPath(String path) {
    ref = _store.collection(path);
  }

  @override
  Stream<List<EventObject>> listenToPostsRealTime() {
    int now = DateTime.now().millisecondsSinceEpoch;
    return ref
        .where("lastUpdate", isGreaterThan: now)
        .snapshots(includeMetadataChanges: true)
        .map((snapshot) => snapshot.docChanges
            .map((docChanged) => convertCommentDocChangeToEvent(docChanged))
            .toList());
  }

  @override
  Future<CommentListWithOffset> getComments<T>(
      T startSnapShot, int limit) async {
    QuerySnapshot result;
    if (startSnapShot == null) {
      result = await ref
          .orderBy("createdDate", descending: true)
          .where("type", isEqualTo: COMMENT_TYPE_PARENT)
          .limit(limit)
          .get();
    } else {
      result = await ref
          .orderBy("createdDate", descending: true)
          .startAfterDocument(startSnapShot as DocumentSnapshot)
          .where("type", isEqualTo: COMMENT_TYPE_PARENT)
          .limit(limit)
          .get();
    }

    final cmtList =
        result.docs.map((doc) => _convertDocSnapshotToComment(doc)).toList();

    DocumentSnapshot? offset;

    if (result.docs.isEmpty) {
      offset = null;
    } else {
      offset = result.docs.last;
    }

    return CommentListWithOffset(commentList: cmtList, offset: offset);
  }

  @override
  Future<void> deleteComment(Comment comment) async {
    try {
      await ref.doc(comment.docId).delete();
    } catch (e) {
      print('deleteComment with docId: ${comment.docId} failed $e');
    }
  }

  @override
  Stream<List<Comment>> getCommentListSnapshotsByParentId(String parentId) {
    return ref
        .where("parentId", isEqualTo: parentId)
        .orderBy("createdDate", descending: false)
        .snapshots()
        .map((querySnapshot) {
      if (querySnapshot.docs.isEmpty) {
        return [];
      } else {
        final childCommentList = querySnapshot.docs.map((docSnapshot) {
          return _convertDocSnapshotToComment(docSnapshot);
        }).toList();
        return childCommentList;
      }
    });
  }

  @override
  Stream<Comment> getCommentSnapshotsById(String id) {
    return ref
        .where("id", isEqualTo: id)
        .orderBy("createdDate", descending: false)
        .limit(1)
        .snapshots()
        .map((docSnapshot) {
      return _convertDocSnapshotToComment(docSnapshot.docs.first);
    });
  }

  Comment _convertDocSnapshotToComment(DocumentSnapshot docSnapshot) {
    Comment item = Comment.fromJson(docSnapshot.data()!);
    item.docId = docSnapshot.id;
    return item;
  }

  @override
  Future<void> insertComment(Comment comment) async {
    try {
      await ref.add(comment.toJson());
      // DocumentReference reference = await ref.add(comment.toJson());
      // return reference.id;
    } catch (error) {
      // return null;
    }
  }

  @override
  Future<void> likeComment(Comment dataLike, String userId) async {
    List likes = [];
    if (userId.isEmpty) {
      return;
    }
    try {
      await ref.doc(dataLike.docId).get().then((value) async {
        var json = value.data()!;
        likes = (json['like'] as List).map((e) => e as String).toList();
        if (likes.contains(userId)) {
          // dislike
          likes.remove(userId);
        } else {
          likes.add(userId);
        }
        await ref.doc(dataLike.docId).update(
          {'like': likes, 'lastUpdate': DateTime.now().millisecondsSinceEpoch},
        );
      });
    } catch (e) {
      print('Phungtd: likeComment failed $e');
    }
  }

  @override
  Future<void> replyComment(String parentId, Comment data) async {
    if (parentId.isNotEmpty && data.parentId != parentId) {
      data.parentId = parentId;
    }
    await ref.add(data.toJson());
  }

  @override
  Future<void> modifyComment(String docId, Comment modify) async {
    await ref.doc(docId).update(
      {
        'content': modify.content,
        'lastUpdate': DateTime.now().millisecondsSinceEpoch
      },
    );
  }
}
