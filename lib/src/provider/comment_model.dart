import 'dart:async';
import 'package:comment/src/constants.dart';
import 'package:comment/src/model/comment.dart';
import 'package:comment/src/model/user.dart';
import 'package:comment/src/service/comment_service.dart';
import 'package:comment/src/service/service.dart';
import 'package:flutter/material.dart';
import 'package:models/module_models.dart';

class CommentModel extends ChangeNotifier {
  CommentService apiService = CommentServiceInitializer().commentService;

  int limit = 10;
  dynamic offset;
  List<Comment> comments = [];
  UserInfo userInfo = UserInfo();

  // AppSetting? appSetting;
  bool isLoading = false;
  bool isFirstTime = true;
  Map<String, dynamic> mapHasChild = {};
  StreamSubscription? commentSubStream;

  // CommentService get apiService => _apiService;
  String? parentId;
  bool refreshParentData = false;

  CommentModel([AppSetting? _appSetting]) {
    if (_appSetting != null) {
      // appSetting = _appSetting;
      userInfo.id = _appSetting.userId;
      userInfo.userName = _appSetting.userName;
    }
  }

  updateUserInfo(UserInfo _userInfo) {
    userInfo = _userInfo;
    notifyListeners();
  }

  void listenToPosts() {
    refreshParentData = false;
    if (commentSubStream == null) {
      commentSubStream = apiService
          .listenToPostsRealTime()
          .listen((List<EventObject> snapshot) {
        if (isFirstTime) {
          return;
        }
        if (snapshot.isNotEmpty) {
          snapshot.forEach((itemChanged) {
            final Comment comment = itemChanged.data;
            switch (itemChanged.type) {
              case ChangeType.added:
                if (comment.type == COMMENT_TYPE_REPLY) {
                  print("add child comment : ${comment.parentId}");
                  comments.forEach((element) {
                    if (comment.parentId == element.id) {
                      element.replyComment.add(comment);
                    }
                  });
                } else {
                  print("add parent comment : ${comment.lastUpdate}");
                  int indexExisted = comments.indexWhere((element) => element.id == comment.id);
                  if (indexExisted == -1) {
                    comments.insert(0, comment);
                  }
                }
                break;
              case ChangeType.modified:
                // trong truong hop nay firebase tra ve 2 ban ghi nen can check hasPendingWrites
                print('Phungtd: a comment has been modified, hasPendingWrites: ${itemChanged.metadata.hasPendingWrites}');
                if (!itemChanged.metadata.hasPendingWrites) {
                  final int indexChanged = comments
                      .indexWhere((element) => element.id == comment.id);
                  print('Phungtd: index of modified comment: $indexChanged');
                  if (indexChanged != -1) {
                    comments[indexChanged] = comment;
                  }
                }
                break;
              case ChangeType.removed:
                comments.removeWhere((element) => element.id == comment.id);
                break;
              default:
                return;
            }
            notifyListeners();
          });
        }
      });
    }
  }

  // setCollectionRef(String path) {
  //   // _apiService.setPath(path);
  // }

  // TODO add service (not necessary)
  // Stream<QuerySnapshot> fetchProductsAsStream() {
  //   return _apiService.streamDataCollection();
  // }

  // Future getCommentCount(String _parentId) async {
  //   // TODO add service (not necessary)
  //   // int count = await _apiService.getDocumentCount(_parentId);
  // }

  Future refreshParentComm() async {
    refreshParentData = true;
    notifyListeners();
  }

  Future getComments() async {
    Map<String, Comment> temp = {};
    isLoading = true;
    notifyListeners();

    final commentListResult = await apiService.getComments(offset, limit);
    isLoading = false;
    List<Comment> commentList = commentListResult.commentList;
    print("DATAAAAA ${commentList.length}");
    if (isFirstTime) {
      isFirstTime = false;
    }
    if (commentList.isEmpty) {
      return;
    }
    if (comments.isEmpty) {
      // this is the first time load comments
      commentList.forEach((item) {
        if (item.type == COMMENT_TYPE_PARENT) {
          comments.add(item);
        } else {
          temp.putIfAbsent(item.parentId, () => item);
        }
      });
    } else {
      // this is load more
      comments.addAll(commentList);
    }
    comments.forEach((element) {
      if (temp.containsKey(element.id)) {
        element.replyComment.add(temp[element.id]!);
      }
    });
    offset = commentListResult.offset;
    if (refreshParentData) {
      refreshParentData = false;
    }
    notifyListeners();
  }

  // ignore: missing_return
  Future<void> insert(Comment comment) async {
    comment.content = comment.content.trim();
    if (comment.content.isEmpty) {
      return;
    }
    apiService.insertComment(comment);
  }

  Future<void> delete(Comment comm) async {
    await apiService.deleteComment(comm);
  }

  // ignore: missing_return
  Future<void> reply(String parentId, Comment replyCom) async {
    replyCom.content = replyCom.content.trim();
    if (replyCom.content.isEmpty) {
      return;
    }
    try {
      await apiService.replyComment(parentId, replyCom);
    } catch (error) {
      print("reply error: $error");
    }
  }

  Future<void> modify(String docId, Comment modifyComm) async {
    modifyComm.content = modifyComm.content.trim();
    if (modifyComm.content.isEmpty) {
      return;
    }
    try {
      await apiService.modifyComment(docId, modifyComm);
    } catch (error) {
      print("reply error: $error");
    }
  }

  Future<void> like(Comment dataLike, String userId) async {
    apiService.likeComment(dataLike, userId);
  }

  reset() {
    isLoading = false;
    offset = null;
    comments = [];
    isFirstTime = true;
    mapHasChild = {};
    commentSubStream?.cancel();
    commentSubStream = null;
  }
}
